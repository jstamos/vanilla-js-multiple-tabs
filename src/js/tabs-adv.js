/**
 * Created by Jessica Stamos (https://jessicastamos.com)
 * Refer to the repo for updates/additional information
 * https://gitlab.com/jnstamos/vanilla-js-multiple-tabs
 * Contact: jessica@jessicastamos.com
 */

const tabsAdv = (() => {
  // Global functions
  const makeArray = (element) => { return Array.prototype.slice.call(element); };
  const hasClass = (element, className) => { return (` ${element.className} `).indexOf(` ${className} `) > -1; };

  // Equal Height begins
  const equalHeight = () => {
    let equalHeightContainer,
        equalHeightChildren,
        allHeights,
        maxHeight;

    equalHeightContainer = document.querySelectorAll('[data-equal-height="true"]');
    for(let i = 0; i < equalHeightContainer.length; i++) {
      allHeights = [];
      maxHeight = -1;
      equalHeightChildren = makeArray(equalHeightContainer[i].children);

      equalHeightChildren.forEach(child => { allHeights.push(child.offsetHeight); });
      maxHeight = Math.max(...allHeights);

      equalHeightChildren.forEach(child => { child.style.height = `${maxHeight}px`; });
    }
  };
  equalHeight();

  // Tabs function
  const getAnimation = element => {
    let animation = element.getAttribute('data-tabs-animation');
    return animation ? ` ${animation}` : '';
  };

  const getTabs = (li) => {
    const tabsContainer = li.parentNode,
          tabsParent = tabsContainer.parentNode,
          contentContainer = tabsParent.querySelector('.tabs__container'),
          tabs = makeArray(tabsContainer.children),
          tabsContent = makeArray(contentContainer.children),
          tabsIndex = tabs.indexOf(li),
          active = makeArray(tabsParent.querySelectorAll('.is-active')),
          animation = getAnimation(tabsParent),
          remember = tabsParent.getAttribute('data-remember-tab');

    if(!hasClass(li, 'is-active')) {
      active.forEach(tab => {
        tab.className = tab.className.replace('is-active', '');
        tab.className = tab.className.replace(animation, '');
      });

      li.className += ' is-active';
      tabsContent[tabsIndex].className += ` is-active${animation}`;
    }

    if(remember) {
      assignRememberTab(remember, tabsIndex);
    }
  };

  const tabsArray = makeArray(document.querySelectorAll('ul.tabs__list > li'));
  tabsArray.forEach(li => { li.addEventListener('click', () => { getTabs(li); }, false); });

  // Remember tabs
  const assignRememberTab = (remember, index) => localStorage.setItem(`lastTab-${remember}`, index);

  const applyRememberTab = () => {
    let tabsContainers = makeArray(document.querySelectorAll('.tabs'));
    tabsContainers.forEach(el => {
      if(el.getAttribute('data-remember-tab')) {
        const tabsList = el.querySelector('ul'),
              tabs = makeArray(tabsList.children),
              tabsContent = makeArray(el.querySelector('.tabs__container').children),
              index = getRememberedTabIndex(el.getAttribute('data-remember-tab')),
              active = makeArray(el.querySelectorAll('.is-active'));

        active.map(activeTab => { activeTab.className = activeTab.className.replace('is-active', ''); });

        tabs[index].className += ' is-active';
        tabsContent[index].className += ' is-active';
      }
    });
  };

  const getRememberedTabIndex = attribute => {
    const index = localStorage.getItem(`lastTab-${attribute}`);
    return index ? index : 0;
  };

  applyRememberTab();
})();